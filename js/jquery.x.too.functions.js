$(function() {
	var e = false,
		p = 0;
	var t = function() {
			$(document).on("click", ".panel ul li", e_options_handler);
			$(document).on("click", ".button-content .button.mute", e_cancel_handler);
			$(document).on("click", "a.condition", e_condition_handler);
			$(document).on("click", ".condition-back", e_condition_handler);
			$(document).on("click", ".button-content .button:not(.mute)", e_acceptsubmit_handler);
			$(document).on("click", ".free_access", e_act_handler)
		};
	e_condition_handler = function(e) {
		$(".panel ul.conditions").toggleClass("hide");
		$(".panel ul.list").toggleClass("hide");
		$("a.condition-back").toggleClass("hide");
		$("a.condition").toggleClass("hide");
		if (!$(".button-content").hasClass("hide")) $(".button-content").addClass("hide");
		$(".panel ul li").removeClass("show").removeClass("hide");
		$(".button-content").addClass("hide")
	};
	e_options_handler = function(e) {
		var t = $(this).data("id"),
			n = $(e.target).parents("ul").find("li");
		if (t.match(/fb/)) {
			p = (t == 'checkin_fb') ? 3:4;
			fb_check_handler();
			return
		}
		if (t.match(/(ema|te)/) == null) return;
			if(t == 'email') p = 1;
			if(t == 'telf') p = 2;
		
		for (var r in n) {
			if (!$.isNumeric(r)) continue;
			if (t !== $(n[r]).data("id")) {
				$(n[r]).addClass("hide")
			} else {
				$(n[r]).addClass("show")
			}
			if (t == "email" || t == "telf") {
				$(".button-content").removeClass("hide")
			}
		}
	};
	e_acceptsubmit_handler = function() {
		$('[name^="email"]').removeClass("error");
		var e = $('[name^="email"]').val(),
			t = $('[name^="telf"]').val(),
			n = e ? "content_email=" + e : "",
			r = t ? "content_telephone=" + t : "",
			i = n + r;
		callData = JSON.stringify({
			serviceName: "newsletter_ddbb",
			methodName: "input",
			parameters: ["opiumbarcelona.com", i]
		});
		$.ajax({
			url: "http://api.dm211.com/?contentType=application/json",
			data: callData,
			type: "POST",
			crossDomain: true,
			success: function(e) {
				switch (e) {
				case 201:
					e_cancel_handler(null, function() 
					{
						e_act_handler()
					});
					break;
				case 226:
				case 400:
				case 444:
					$('[name^="email"]').addClass("error");
					$('[name^="telf"]').addClass("error");
					break
				}
			}
		})
	};
	e_act_handler = function() 
	{
		if(debug) console.log('package ['+p+']');
		
		if(p == 0) return;
		
		$.ajax({
			url: '/guest/login',
			data: [{
				'by': 'credit',
				'package': p,
				'landing_url': fn_hash,
				'page_error': 'index.html'
			}],
			type: 'POST',
			success: function(data, textStatus, jqXHR) {
				//segun lo que responda el router habria que hacer un if
				
				/*if(textStatus)
				{
				*/
					window.location.href = fn_hash;
				/*}
				*/				
				
			}
		});
	};
	fb_check_handler = function() {
		if(debug) console.log("call fb if success -> access garanted");
		
		if(p == 0) return;
		
		/*if()
		{
		*/
			//window.location.href = fn_hash;
		/*}			
		*/
	};
	e_cancel_handler = function(e, t) {
		$('[name^="email"]').removeClass("error");
		$('[name^="telf"]').removeClass("error");
						
		$(".panel ul li").removeClass("show").removeClass("hide");
		$(".button-content").addClass("hide");
		if (t) t.call(this)
	};
	t()
})